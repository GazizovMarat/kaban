﻿namespace Kaban.Entities
{
    public interface IHasName
    {
        string Name { get; set; }
    }
}