﻿namespace Kaban.Entities
{
    public enum CardCategory : int
    {
        ToDo = 1,
        Doing = 2,
        Done = 3
    }
}